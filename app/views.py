from django.http import Http404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)

from .models import (
    Specialization,
    Doctor,
    Patient
)

from .serializers import (
    SpecializationSerializer,
    DoctorSerializer,
    PatientSerializer)


class SpecializationMixin:
    serializer_class = SpecializationSerializer
    queryset = Specialization.objects.all()


class SpecializationListView(SpecializationMixin, ListAPIView):
    pass


class SpecializationCreateView(SpecializationMixin, CreateAPIView):
    pass


class SpecializationRetrieveView(SpecializationMixin, RetrieveAPIView):
    pass


class SpecializationUpdateView(SpecializationMixin, UpdateAPIView):
    pass


class SpecializationDestroyView(SpecializationMixin, DestroyAPIView):
    pass


class DoctorListCreateView(APIView):
    """
        In APIView, need to manually override http methods for corresponding requests.
    """

    def get(self, request, format=None):
        # list
        doctors = Doctor.objects.all()
        serializer = DoctorSerializer(doctors, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        # create
        serializer = DoctorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DoctorRetrieveUpdateDeleteView(APIView):

    def get_object(self, pk):
        try:
            return Doctor.objects.get(pk=pk)
        except Doctor.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        # retrieve
        doctor = self.get_object(pk)
        serializer = DoctorSerializer(doctor)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        # update - put
        doctor = self.get_object(pk)
        serializer = DoctorSerializer(doctor, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        # delete
        doctor = self.get_object(pk)
        doctor.delete()
        return Response({"message": "Deleted successfully!"},
                        status=status.HTTP_204_NO_CONTENT)


class PatientMixin:
    serializer_class = PatientSerializer
    queryset = Patient.objects.all()


class PatientListCreateView(PatientMixin, ListCreateAPIView):
    pass


class PatientRetrieveUpdateDelete(PatientMixin, RetrieveUpdateDestroyAPIView):
    pass
