from rest_framework import serializers

from .models import (
    Specialization,
    Doctor,
    Patient,
    PatientDetail
)


class SpecializationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50)

    class Meta:
        model = Specialization
        fields = ('name',)


class DoctorSerializer(serializers.ModelSerializer):
    specializations = SpecializationSerializer(many=True)

    # A doctor can have many specializations so set many=True

    def create(self, validated_data):
        # for nested serializers create or update, need to override serializers create method
        # and manually save the nested serializer data.
        specializations = validated_data.pop('specializations')
        added_spec = []
        for spec in specializations:
            added_spec.append(
                Specialization.objects.get_or_create(name=spec.get('name'))[0])
            # returns Specialization, True/False. So taking index 0
        doctor = Doctor.objects.create(**validated_data)
        doctor.specializations.add(*added_spec)
        return doctor

    def update(self, instance, validated_data):
        # for nested serializers create or update, need to override serializers create method
        # and manually save the nested serializer data.
        specializations = validated_data.pop('specializations')
        added_spec = []
        for spec in specializations:
            added_spec.append(
                Specialization.objects.get_or_create(name=spec.get('name'))[0])
        instance.specializations.clear()  # clear all previous m2m relations
        instance.specializations.add(*added_spec)
        return super().update(instance, validated_data)

    class Meta:
        model = Doctor
        fields = (
            'specializations', 'name', 'photo', 'address', 'contact', 'remark')


class PatientDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientDetail
        fields = ('photo', 'address', 'contact', 'remark')
        # this serializer is nested by patient serializer so don't require patient field


class PatientSerializer(serializers.ModelSerializer):
    patient_detail = PatientDetailSerializer()

    def create(self, validated_data):
        patient_detail = validated_data.pop('patient_detail')
        patient = Patient.objects.create(**validated_data)
        PatientDetail.objects.create(patient=patient, **patient_detail)
        return patient

    def update(self, instance, validated_data):
        # update the patient detail
        patient_detail = PatientDetail.objects.filter(
            patient=instance.id).update(**validated_data.pop('patient_detail'))

        # update the patient
        return super().update(instance, validated_data)

    class Meta:
        model = Patient
        fields = ('name', 'report', 'health_status', 'patient_detail')
