from django.urls import path
from .views import (
    DoctorListCreateView,
    DoctorRetrieveUpdateDeleteView,
    SpecializationCreateView,
    SpecializationListView,
    SpecializationRetrieveView,
    SpecializationUpdateView,
    SpecializationDestroyView,
    PatientListCreateView,
    PatientRetrieveUpdateDelete
)

urlpatterns = [
    path('doctor/', DoctorListCreateView.as_view()), # APIView
    path('doctor/<int:pk>/', DoctorRetrieveUpdateDeleteView.as_view()), # APIView

    path('specialization/create/', SpecializationCreateView.as_view()), # CreateAPIView
    path('specialization/list/', SpecializationListView.as_view()), # ListAPIView
    path('specialization/<int:pk>/retrieve/',
         SpecializationRetrieveView.as_view()), # RetrieveAPIView - requires pk
    path('specialization/<int:pk>/udpate/', SpecializationUpdateView.as_view()), # UpdateAPIView - requires pk
    path('specialization/<int:pk>/delete/',
         SpecializationDestroyView.as_view()), # DestroyAPIView - requires pk

    path('patient/', PatientListCreateView.as_view()), # ListCreateAPIView
    path('patient/<int:pk>/', PatientRetrieveUpdateDelete.as_view()) # RetrieveUpdateDestroyAPIView - requires pk
]
