"""

    A Doctor can have many Specializations.
    A specialization can be acquired by many Doctors.
    Doctor <----------> Specialization


    A Patient can have only one Doctor.
    A Doctor can have many Patients.
    Doctor |----------> Patient


    A Patient can have only one PatientDetail.
    A PatientDetail belongs to only one Patient.
    Patient |-------------| PatientDetail

"""
from django.db import models


class Specialization(models.Model):
    name = models.CharField(unique=True, max_length=100)
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name


class Doctor(models.Model):
    specializations = models.ManyToManyField(Specialization,
                                             related_name='doctors')
    name = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='uploads/doctors', blank=True)
    address = models.CharField(max_length=100)
    contact = models.CharField(max_length=25)
    remark = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name


class Patient(models.Model):
    doctor = models.ForeignKey(Doctor, related_name='patients', null=True,
                               on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    report = models.FileField(upload_to='uploads/reports', blank=True)
    health_status = models.BooleanField()  # True for healthy, False for unhealthy

    def __str__(self):
        return self.name


class PatientDetail(models.Model):
    patient = models.OneToOneField(Patient, related_name='patient_detail', on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='uploads/clients', blank=True)
    address = models.CharField(max_length=100)
    contact = models.CharField(max_length=25)
    remark = models.TextField(blank=True, default='')

    def __str__(self):
        return "Patient detail of: " + self.patient.name
